<?php

namespace ATM\OrderTrackerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use \DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="atm_purchase")
 */
class Purchase{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    protected $creationDate;

    /**
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    protected $email;

    /**
     * @ORM\Column(name="address", type="text", nullable=false)
     */
    protected $address;

    /**
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    protected $city;

    /**
     * @ORM\Column(name="state", type="string", length=255, nullable=true)
     */
    protected $state;

    /**
     * @ORM\Column(name="zip_code", type="string", length=10, nullable=false)
     */
    protected $zipCode;

    /**
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    protected $country;

    /**
     * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
     */
    protected $first_name;

    /**
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    protected $last_name;

    /**
     * @ORM\Column(name="delivery_instructions", type="text", nullable=true)
     */
    protected $delivery_instructions;

    /**
     * @ORM\OneToMany(targetEntity="PurchaseProductSize",mappedBy="purchase")
     */
    protected $purchasedProductSizes;

    /**
     * @ORM\Column(name="total",type="decimal", precision=7, scale=2)
     */
    private $total;

    /**
     * @ORM\Column(name="discount",type="decimal", precision=7, scale=2, nullable=true)
     */
    private $totalWithDiscount;

    /**
     * @ORM\Column(name="sent", type="boolean", nullable=true)
     */
    private $sent;

    /**
     * @ORM\Column(name="shipmentCost",type="decimal", precision=7, scale=2, nullable=true)
     */
    private $shipmentCost;

    /**
     * @ORM\Column(name="tracking_code", type="string", length=255, nullable=true)
     */
    private $trackingCode;

    /**
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phoneNumber;

    /**
     * @ORM\Column(name="send_date", type="datetime", nullable=true)
     */
    private $sendDate;

    /**
     * @ORM\OneToOne(targetEntity="Image")
     */
    protected $image;

    private $giftedTo;

    /**
     * @ORM\Column(name="anonymous_gift", type="boolean", nullable=true)
     */
    private $anonymous_gift;

    protected $user;

    public function __construct()
    {
        $this->creationDate = new DateTime();
        $this->purchasedProductSizes = new ArrayCollection();
        $this->total = 0;
        $this->totalWithDiscount = 0;
        $this->shipmentCost = 0;
        $this->sent = false;
        $this->anonymous_gift = false;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getCreationDate()
    {
        return $this->creationDate;
    }

    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function getState()
    {
        return $this->state;
    }

    public function setState($state)
    {
        $this->state = $state;
    }

    public function getZipCode()
    {
        return $this->zipCode;
    }

    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    public function getDeliveryInstructions()
    {
        return $this->delivery_instructions;
    }

    public function setDeliveryInstructions($delivery_instructions)
    {
        $this->delivery_instructions = $delivery_instructions;
    }

    public function getPurchasedProductSizes()
    {
        return $this->purchasedProductSizes;
    }

    public function addPurchasedProductSizes($purchasedProductSizes)
    {
        $this->purchasedProductSizes->add($purchasedProductSizes);
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total)
    {
        $this->total = $total;
    }

    public function getTotalWithDiscount()
    {
        return $this->totalWithDiscount;
    }

    public function setTotalWithDiscount($totalWithDiscount)
    {
        $this->totalWithDiscount = $totalWithDiscount;
    }

    public function getShipmentCost()
    {
        return $this->shipmentCost;
    }

    public function setShipmentCost($shipmentCost)
    {
        $this->shipmentCost = $shipmentCost;
    }

    public function getSent()
    {
        return $this->sent;
    }

    public function setSent($sent)
    {
        $this->sent = $sent;
    }

    public function getTrackingCode()
    {
        return $this->trackingCode;
    }

    public function setTrackingCode($trackingCode)
    {
        $this->trackingCode = $trackingCode;
    }

    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getSendDate()
    {
        return $this->sendDate;
    }

    public function setSendDate($sendDate)
    {
        $this->sendDate = $sendDate;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getGiftedTo()
    {
        return $this->giftedTo;
    }

    public function setGiftedTo($giftedTo)
    {
        $this->giftedTo = $giftedTo;
    }

    public function getAnonymousGift()
    {
        return $this->anonymous_gift;
    }

    public function setAnonymousGift($anonymous_gift)
    {
        $this->anonymous_gift = $anonymous_gift;
    }
}