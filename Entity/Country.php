<?php

namespace ATM\OrderTrackerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="atm_country")
 */
class Country{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * @ORM\Column(name="code", type="string", length=5, nullable=false)
     */
    protected $code;

    /**
     * @ORM\OneToOne(targetEntity="ShipmentCost", mappedBy="country")
     */
    protected $shipmentCost;

    /**
     * @ORM\Column(name="phone_code", type="string", length=255, nullable=true)
     */
    private $phoneCode;

    /**
     * @ORM\Column(name="hidden", type="boolean", nullable=false)
     */
    private $hidden;

    public function __construct()
    {
        $this->hidden = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getShipmentCost()
    {
        return $this->shipmentCost;
    }

    public function setShipmentCost($shipmentCost)
    {
        $this->shipmentCost = $shipmentCost;
    }

    public function getPhoneCode()
    {
        return $this->phoneCode;
    }

    public function setPhoneCode($phoneCode)
    {
        $this->phoneCode = $phoneCode;
    }

    public function getHidden()
    {
        return $this->hidden;
    }

    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }
}