<?php

namespace ATM\OrderTrackerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $treeBuilder
            ->root('atm_order_tracker')
                ->children()
                    ->scalarNode('user')->isRequired()->end()
                    ->scalarNode('user_role')->defaultValue(null)->isRequired()->end()
                    ->scalarNode('media')->isRequired()->end()
                    ->scalarNode('user_field_name')->defaultValue('user')->isRequired()->end()
                    ->scalarNode('media_folder')->isRequired()->end()
                    ->scalarNode('route_after_purchase')->isRequired()->end()
                    ->scalarNode('minimum_points_for_purchase')->isRequired()->end()
                    ->scalarNode('minimum_product_tokens_for_adding_to_wishlist')->defaultValue(500)->end()
                    ->arrayNode('user_media_roles')->prototype('scalar')->defaultValue('[]')->end()->end()
        ;

        return $treeBuilder;
    }
}
