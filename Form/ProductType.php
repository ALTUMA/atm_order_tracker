<?php

namespace ATM\OrderTrackerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use ATM\OrderTrackerBundle\Entity\Product;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,array(
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Name',
                    'autocomplete' => 'off'
                )
            ))
            ->add('price',MoneyType::class,array(
                'required' => true,
                'currency' => null,
                'attr' => array(
                    'placeholder' => 'Points',
                    'autocomplete' => 'off'
                )
            ))
            ->add('tokens',MoneyType::class,array(
                'required' => true,
                'currency' => null,
                'attr' => array(
                    'placeholder' => 'Tokens',
                    'autocomplete' => 'off'
                )
            ))
            ->add('init_date',DateTimeType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                    'placeholder' => 'Init Date',
                    'autocomplete' => 'off'
                ),
            ))
            ->add('end_date',DateTimeType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                    'placeholder' => 'End Date',
                    'autocomplete' => 'off'
                ),
            ))
            ->add('sizes', EntityType::class, array(
                'class' => 'ATM\OrderTrackerBundle\Entity\Size',
                'query_builder' => function(EntityRepository $er) {
                    $qb = $er->createQueryBuilder('s');
                    return $qb
                        ->orderBy('s.position', 'ASC');
                },
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true,
                'empty_data'  => null,
                'required' => false
            ))
            ->add('description',TextareaType::class,array(
                'required' => false
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Product::class,
        ));
    }

    public function getName()
    {
        return 'atmorder_tracker_bundle_product_type';
    }
}