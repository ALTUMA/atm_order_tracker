A Order Tracker Bundle to manage orders made by the users

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require atm/ordertrackerbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new ATM\OrderTrackerBundle\ATMOrderTrackerBundle(),
    ];
}
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
atm_order_tracker:
    user: Namespace of your user class
    media: Namespace of the media for the points manager
    user_field_name: Name of the user field in your media class
    media_folder: path to folder where images are going to be stored
    route_after_purchase: route name after purchase form
    minimum_points_for_purchase: Minimum points needed for purchasing items in the shop
    user_media_roles: Roles of the users that have media
```


## Include inside a view to load a popup with all the products for the user to chose
```twig
{{ render(controller('ATMOrderTrackerBundle:Product:seeProductsForPurchase' , {
    'page' : 1,
    'max_results': 10
})) }}
```
The default way to show the products is in a popup but you can change it overwriting
the view OrderTrackerBundle/views/Purchase/do_purchase.html.twig.


## Events

There is 1 event that is thrown after a purchase is made:
 
```php
class PostPurchase extends Event{
    const NAME = 'atm_order_tracker_post_purchase.event';
    private $purchase;
    private $user;

    public function __construct($user,$purchase)
    {
        $this->user = $user;
        $this->purchase = $purchase;
    }

    public function getPurchase()
    {
        return $this->purchase;
    }

    public function getUser()
    {
        return $this->user;
    }
}
```