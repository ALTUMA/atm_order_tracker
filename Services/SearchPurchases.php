<?php

namespace ATM\OrderTrackerBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use \DateTime;

class SearchPurchases{

    private $em;
    private $paginator;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function search($options){
        $defaultOptions = array(
            'ids' => null,
            'email' => null,
            'address' => null,
            'city' => null,
            'state' => null,
            'zip_code' => null,
            'country' => null,
            'first_name' => null,
            'last_name' => null,
            'sent' => null,
            'phone_number' => null,
            'product_name' => null,
            'max_results' => null,
            'pagination' => null,
            'date_range' => array(
                'init_date' => null,
                'end_date' => null
            ),
            'page' => 1,
            'order_by_field' => 'creationDate',
            'order_by_direction' => 'DESC',
            'user_ids' => null,
            'show_only_gifts' => false,
            'gifted_to' => null
        );

        $options = array_merge($defaultOptions, $options);

        $qbIds = $this->em->createQueryBuilder();
        $qbIds
            ->select('partial p.{id}')
            ->from('ATMOrderTrackerBundle:Purchase','p');

        $andX = $qbIds->expr()->andX();

        if(!is_null($options['email'])){
            $andX->add($qbIds->expr()->like('p.email',$qbIds->expr()->literal('%'.$options['email'].'%')));
        }

        if(!is_null($options['address'])){
            $andX->add($qbIds->expr()->like('p.address',$qbIds->expr()->literal('%'.$options['address'].'%')));
        }

        if(!is_null($options['city'])){
            $andX->add($qbIds->expr()->like('p.city',$qbIds->expr()->literal('%'.$options['city'].'%')));
        }

        if(!is_null($options['state'])){
            $andX->add($qbIds->expr()->like('p.state',$qbIds->expr()->literal('%'.$options['state'].'%')));
        }

        if(!is_null($options['zip_code'])){
            $andX->add($qbIds->expr()->like('p.zipCode',$qbIds->expr()->literal('%'.$options['zip_code'].'%')));
        }

        if(!is_null($options['country'])){
            $qbIds->join('p.country','c','WITH',$qbIds->expr('c.name',$qbIds->expr()->like($qbIds->expr()->literal('%'.$options['country'].'%'))));
        }

        if(!is_null($options['first_name'])){
            $andX->add($qbIds->expr()->like('p.country',$qbIds->expr()->literal('%'.$options['first_name'].'%')));
        }

        if(!is_null($options['last_name'])){
            $andX->add($qbIds->expr()->like('p.last_name',$qbIds->expr()->literal('%'.$options['last_name'].'%')));
        }

        if(!is_null($options['sent'])){
            $andX->add($qbIds->expr()->eq('p.sent',($options['sent'] === true) ? 1 : 0));
        }

        if(!is_null($options['last_name'])){
            $andX->add($qbIds->expr()->like('p.phone_number',$qbIds->expr()->literal('%'.$options['phone_number'].'%')));
        }

        if(!is_null($options['product_name'])){
            $qbIds->join('p.purchasedProductSizes','pps','WITH',$qbIds->expr()->like('pps.productName',$qbIds->expr()->literal('%'.$options['product_name'].'%')));
        }

        if(!is_null($options['ids'])){
            $andX->add($qbIds->expr()->in('p.id',$options['ids']));
        }

        if(!is_null($options['date_range']['init_date'])){
            $init_date = new DateTime($options['date_range']['init_date']);
            $andX->add(
                $qbIds->expr()->gte('p.creationDate', $qbIds->expr()->literal($init_date->format('Y-m-d').' 00:00:00'))
            );
        }

        if(!is_null($options['date_range']['end_date'])){
            $end_date = new DateTime($options['date_range']['end_date']);
            $andX->add(
                $qbIds->expr()->lte('p.creationDate', $qbIds->expr()->literal($end_date->format('Y-m-d').' 23:59:59'))
            );
        }

        if(!is_null($options['user_ids'])){
            $qbIds->join('p.user','u','WITH',$qbIds->expr()->in('u.id',$options['user_ids']));
        }


        if($options['show_only_gifts'] === true){
            $qbIds->join('p.giftedTo','gt');
        }elseif(!is_null($options['gifted_to'])){
            $qbIds->join('p.giftedTo','gt','WITH',$qbIds->expr()->eq('gt.id',$options['gifted_to']));
        }

        if($andX->count() > 0){
            $qbIds->where($andX);
        }

        $qbIds->orderBy('p.'.$options['order_by_field'],$options['order_by_direction']);

        $pagination = null;
        if(!is_null($options['pagination'])){
            $arrIds = array_map(function($p){
                return $p['id'];
            },$qbIds->getQuery()->getArrayResult());

            $pagination = $this->paginator->paginate(
                $arrIds,
                is_null($options['page']) ? 1 : $options['page'],
                is_null($options['max_results']) ? 10 : $options['max_results']
            );

            $ids = $pagination->getItems();
        }else {
            $query = $qbIds->getQuery();
            if(!is_null($options['max_results'])){
                $query->setMaxResults($options['max_results']);
            }

            $ids = array_map(function ($p) {
                return $p['id'];
            }, $query->getArrayResult());
        }

        $purchases = array();
        if(count($ids) > 0){
            $qb = $this->em->createQueryBuilder();
            $qb
                ->select('partial p.{id,creationDate,email,address,city,state,zipCode,first_name,last_name,delivery_instructions,total,sent,shipmentCost,trackingCode,phoneNumber,sendDate}')
                ->addSelect('partial i.{id,path}')
                ->addSelect('partial c.{id,name,code,phoneCode}')
                ->addSelect('partial u.{id,username,usernameCanonical}')
                ->addSelect('partial pps.{id,productName,price,tokens}')
                ->addSelect('partial s.{id,name}')
                ->addSelect('partial gt.{id,username,usernameCanonical}')
                ->from('ATMOrderTrackerBundle:Purchase','p')
                ->leftJoin('p.image','i')
                ->leftJoin('p.purchasedProductSizes','pps')
                ->leftJoin('p.country','c')
                ->leftJoin('p.giftedTo','gt')
                ->join('p.user','u')
                ->leftJoin('pps.size','s')
                ->where($qb->expr()->in('p.id',$ids))
                ->orderBy('p.creationDate','DESC');

            $purchases = $qb->getQuery()->getArrayResult();
        }


        return array(
            'results' => $purchases,
            'pagination' => $pagination
        );

    }
}