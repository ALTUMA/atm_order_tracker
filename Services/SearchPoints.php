<?php

namespace ATM\OrderTrackerBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;

class SearchPoints{

    private $em;
    private $configuration;
    private $paginator;

    const CONTENT_TYPE_TEXT = 1;
    const CONTENT_TYPE_IMAGE = 2;
    const CONTENT_TYPE_LINK = 3;
    const CONTENT_TYPE_VIDEO = 4;
    const CONTENT_TYPE_GIF = 5;

    public function __construct(EntityManagerInterface $em,PaginatorInterface $paginator,$atm_order_tracker_config)
    {
        $this->em = $em;
        $this->paginator = $paginator;
        $this->configuration = $atm_order_tracker_config;
    }

    public function search($options){
        $defaultOptions = array(
            'ids' => null,
            'media_type' => array(
                self::CONTENT_TYPE_TEXT,
                self::CONTENT_TYPE_IMAGE,
                self::CONTENT_TYPE_LINK,
                self::CONTENT_TYPE_VIDEO,
                self::CONTENT_TYPE_GIF
            ),
            'user_id' => null,
            'max_results' => null,
            'pagination' => null,
            'date_range' => array(
                'init_date' => null,
                'end_date' => null
            ),
            'order_by_field' => 'creation_date',
            'order_by_direction' => 'DESC',
            'page' => 1,
        );

        $options = array_merge($defaultOptions, $options);

        $qbIds = $this->em->createQueryBuilder();

        $qbIds
            ->select('p')
            ->from('ATMOrderTrackerBundle:Points','p');


        $andX = $qbIds->expr()->andX();
        if(!is_null($options['ids'])){
            $andX->add($qbIds->expr()->in('p.id',$options['ids']));
        }

        if(!is_null($options['content_type'])){
            $qbIds->join('p.media','m','WITH',$qbIds->expr()->in('m.content_type', $options['content_type']));
        }

        if(!is_null($options['user_id'])){

            if(is_null($options['content_type'])){
                $qbIds->join('p.media','m');
            }

            $qbIds->join('m.'.$this->configuration['user_field_name'],'u','WITH',$qbIds->expr()->eq('u.id',$options['user_id']));
        }

        if(!is_null($options['date_range']['init_date'])){
            $init_date = new DateTime($options['date_range']['init_date']);
            $andX->add(
                $qbIds->expr()->gte('p.creation_date', $qbIds->expr()->literal($init_date->format('Y-m-d')))
            );
        }

        if(!is_null($options['date_range']['end_date'])){
            $end_date = new DateTime($options['date_range']['end_date']);
            $andX->add(
                $qbIds->expr()->lte('p.creation_date', $qbIds->expr()->literal($end_date->format('Y-m-d')))
            );
        }

        if($andX->count() > 0){
            $qbIds->where($andX);
        }

        $qbIds->orderBy('p.'.$options['order_by_field'],$options['order_by_direction']);


        $pagination = null;
        if(!is_null($options['pagination'])){
            $arrIds = array_map(function($p){
                return $p['id'];
            },$qbIds->getQuery()->getArrayResult());

            $pagination = $this->paginator->paginate(
                $arrIds,
                is_null($options['page']) ? 1 : $options['page'],
                is_null($options['max_results']) ? 10 : $options['max_results']
            );

            $ids = $pagination->getItems();
        }else {
            $query = $qbIds->getQuery();
            if(!is_null($options['max_results'])){
                $query->setMaxResults($options['max_results']);
            }

            $ids = array_map(function ($p) {
                return $p['id'];
            }, $query->getArrayResult());
        }

        $points = array();
        if(count($ids) > 0){
            $qb = $this->em->createQueryBuilder();

            $qb
                ->select('p')
                ->addSelect('m')
                ->addSelect('u')
                ->from('ATMOrderTrackerBundle:Points','p')
                ->join('p.media','m')
                ->join('m.'.$this->configuration['user_field_name'],'u')
                ->where($qb->expr()->in('p.id',$ids))
                ->orderBy('p.'.$options['order_by_field'],$options['order_by_direction'])
            ;

            $points = $qb->getQuery()->getArrayResult();
        }

        return array(
            'results' => $points,
            'pagination' => $pagination
        );
    }
}